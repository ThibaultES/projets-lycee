regions = \
        { 'Lille' : [ ('Rouen', 254), ('Paris', 217), ('Strasbourg', 522) ], \
          'Rouen' : [ ('Lille',254), ('Paris',136), ('Rennes', 312), ('Nantes',383), ('Orléans', 211) ],\
          'Paris' : [ ('Lille',217),  ('Rouen',136), ('Strasbourg', 487),('Orléans',131), ('Dijon', 314)],\
          'Strasbourg' : [ ('Lille', 522), ('Paris',487), ('Dijon',332)],\
          'Rennes' : [  ('Rouen',312), ('Nantes',106)],\
          'Nantes' : [  ('Rouen', 383), ('Rennes',106) ,('Orléans', 334), ('Bordeaux',347)],\
          'Orléans' : [  ('Rouen',211), ('Paris', 131), ('Nantes', 334), ('Dijon', 315),('Bordeaux', 463), ('Lyon', 450)],\
          'Dijon' : [  ('Paris',314), ('Strasbourg', 332), ('Orléans', 315), ('Lyon', 192)],\
          'Bordeaux' : [ ('Nantes', 347), ('Orléans',463), ('Lyon', 552), ('Toulouse', 246) ], \
          'Lyon' : [  ('Orléans',450), ('Dijon',192),('Bordeaux',552), ('Toulouse', 537), ('Marseille',313) ], \
          'Toulouse' :  [ ('Bordeaux',246), ('Lyon',537), ('Marseille',403) ], \
          'Marseille' : [ ('Lyon',313), ('Toulouse',403) ] \
          }
          
          
